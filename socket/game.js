import {listOfUsers} from "../db";
import {READY_STATE, NOT_READY_STATE} from "./config";
export default (socket, username) => {
  
    socket.emit("RENDER_LIST_OF_USERS", listOfUsers, username);
    socket.broadcast.emit("ADD_NEW_USER", username, "not-ready");

    socket.on("READY_NOT_READY_STATE", (state) => {
        switch(state) {
          case(READY_STATE):
            state = NOT_READY_STATE;
            listOfUsers.forEach( user => {
              if(user.username === username){
                user.status = "ready";
              }
            });
            break;
          case(NOT_READY_STATE):
            state = READY_STATE;
            listOfUsers.forEach( user => {
              if(user.username === username){
                user.status = "not-ready";
              }
            });
            break;
          default: 
            break;
        }
        socket.emit("CHANGE_BUTTON_STATE", state);
        socket.emit("CHANGE_STATUS_BLOCK", listOfUsers, username);
        socket.broadcast.emit("CHANGE_STATUS_BLOCK", listOfUsers, username);
    });
    
}

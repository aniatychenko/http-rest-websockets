import * as config from "./config";
import data from "../data"
import {listOfUsers, listOfUsersRating} from "../db";
import gameHandler from "./game"
let finish = false;
export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    const usernames = listOfUsers.map(user => {return user.username})
    if(usernames.includes(username))
    {
      socket.emit("USERNAME_ERROR", username);
      return;
    }
    else if(listOfUsers.length === config.MAXIMUM_USERS_FOR_ONE_ROOM){
      socket.emit("ROOM_ERROR");
      return;
    }
    else {
      listOfUsers.push({username, status: "not-ready"});
    }
    gameHandler(socket, username);

    socket.on("ARE_ALL_USERS_READY_TO_PLAY", () => {
      let isReady = true;
      listOfUsers.forEach( user => {
        if(user.status === "not-ready"){
          isReady = false;
          return;
        }
      });
      if(!isReady)
      {
        return;
      }
      startTimerBeforeGame(io);
    });
    socket.on("START_GAME", (text) => {
      let place = 1;
      listOfUsersRating.clear();
      let nextLetter = text[0];
      let progressPersents = 0;
      let typedText = "";
      let indexOfNextLetter = 0;
      startTimerInGame(io, socket);
      listOfUsers.forEach(user => {
        listOfUsersRating.set(user.username, 0);
      })
      socket.on("TYPING_TEXT", (userPressed) =>{
        if(userPressed === nextLetter)
        {
          indexOfNextLetter++;
          typedText += nextLetter;
          progressPersents = Math.floor(typedText.length * 100 / text.length);
          // if(progressPersents === 100)
          // {
          //   listOfUsers.forEach(user => {
          //     if(user.username = username)
          //     {
          //       listOfUsers.place = place;
          //       place ++;
          //     }
          //   })
          // }
          nextLetter = text[indexOfNextLetter];
          if(!nextLetter) {
            nextLetter = "";
          }
          listOfUsersRating.set(username, progressPersents);
          socket.emit("DRAW_TEXT",text, indexOfNextLetter, typedText);
          io.emit("DRAW_INDICATOR", progressPersents, listOfUsers, username)
          if(Array.from(listOfUsersRating.values()).every(progress =>  progress === 100))
          {
            finish = true;
            place = 1;
            listOfUsers.forEach((user) => {
              user.status = "not-ready";
            })
            io.emit("END_GAME", listOfUsers);
          }
        }
      })

      
    });
    socket.on("CLEAR_RATING", () => {
      listOfUsersRating.length = 0;
    });
    socket.on("disconnect", () => {
      console.log(username);
      const index = listOfUsers.findIndex(user => user.username === username);
        if (index > -1) {
          listOfUsers.splice(index, 1);
          io.emit("RENDER_LIST_OF_USERS_NEW", listOfUsers);
    }
      console.log(`${socket.id} disconnected`);
    });
  });
};


const startTimerBeforeGame = (io) => {
  let seconds = config.SECONDS_TIMER_BEFORE_START_GAME;
  seconds--;
  const textId = Math.floor(Math.random()*(data.texts.length - 0));
  io.emit("SET_TIMER_BEFORE_START", seconds);
  const interval = setInterval(() => {
  io.emit("RENDER_SECONDS_LEFT", seconds);
  if (seconds <= 0) {
    clearInterval(interval);
    io.emit("SET_TEXT", textId);
  }
  seconds--;
  }, 1000);
}
const startTimerInGame = (io, socket) => {
  let seconds = config.SECONDS_FOR_GAME;
  seconds--;
  io.emit("SET_GAME_TIMER", seconds);
  const interval = setInterval(() => {
    if(finish){
      clearInterval(interval);
      return;
    }
  io.emit("RENDER_GAME_SECONDS_LEFT", seconds);
  if (seconds <= 0) {
    clearInterval(interval);
    const sortedRating = new Map([...listOfUsersRating.entries()].sort((a, b) => b[1] - a[1]));
    const ratingArray = Array.from(sortedRating);
    listOfUsers.forEach((user) => {
      user.status = "not-ready";
    })
    socket.emit("END_GAME_TIME", ratingArray, listOfUsers);
  }
  seconds--;
  }, 1000);
}
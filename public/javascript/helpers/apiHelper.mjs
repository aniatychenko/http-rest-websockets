const API_URL = '';

async function callApi(endpoint, method) {
    const url = API_URL + endpoint;
    const options = {
        method,
    };
    return fetch(url, options)
        .then((response) => (response.ok ? response.json() : Promise.reject(Error('Failed to load'))))
        .catch((error) => {
            throw error;
        });
}

export async function getTextForGameById(id) {
    return await callApi(`/game/texts/${id}`, 'GET');
}

export { callApi };
import {getTextForGameById} from "./helpers/apiHelper.mjs"
const username = sessionStorage.getItem("username");
const playersBlock = document.getElementsByClassName("players-block")[0];
const readyButton = document.getElementById("ready-button");
const disconnectButton = document.getElementById("disconnect-button");
const timerBeforeStart = document.getElementsByClassName("timer-before-start")[0];
const textField = document.getElementsByClassName("text-for-game")[0];
const typedText = document.getElementsByClassName("typed-text")[0];
const nextLetter = document.getElementsByClassName("next-letter")[0];
const restText = document.getElementsByClassName("rest-text")[0];
const timerForGame = document.getElementsByClassName("time-for-game")[0];
const ratingModal = document.getElementsByClassName("rating-modal")[0];
const closeButton = document.getElementsByClassName("close-button")[0];


if (!username) {
  window.location.replace("/login");
}
const socket = io("", { query: { username } });

const createElement = (tagName, className, text) => {
  const element = document.createElement(tagName);
  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }
  if(text)
  {
    element.innerText = text;
  }
  return element;
}

const usernameISNotValid = (username) => {
    sessionStorage.removeItem("username");
    window.location.replace("/login");
    alert(`Player with name ${username} is already in game`);
  }
const roomLimitExceeded = () => {
    sessionStorage.removeItem("username");
    window.location.replace("/login");
    alert("This room already has maximum amount of users");
  }
const renderUsers = (userList, username) => {
  playersBlock.innerHTML = "";
    userList.forEach(user => {
      user.username === username ? addNewUser(user.username + " (you)", user.status) : addNewUser(user.username, user.status);
  });
}
const renderNewUsers = (userList) => {
  playersBlock.innerHTML = "";
    userList.forEach(user => {
      user.username === username ? addNewUser(user.username + " (you)", user.status) : addNewUser(user.username, user.status);
  });
  socket.emit("ARE_ALL_USERS_READY_TO_PLAY");
}

const addNewUser = (username, status) => {
  const mainPlayerInfoDiv = createElement("div", "player-info flex");
  const statusAndUsernameField = createElement("div", "status-name-field flex");
  const statusField = createElement("div", `player-status ${status}`);
  const userNameField = createElement("span", "player-name", username);
  const progressBar = createElement("div", "progress-bar");
  const progressIndicator = createElement("div", "progress-indicator");

  progressBar.appendChild(progressIndicator);
  statusAndUsernameField.appendChild(statusField);
  statusAndUsernameField.appendChild(userNameField);
  mainPlayerInfoDiv.appendChild(statusAndUsernameField);
  mainPlayerInfoDiv.appendChild(progressBar);
  playersBlock.appendChild(mainPlayerInfoDiv);
}
const changeButton = (state) => {
  readyButton.textContent = state;
  socket.emit("ARE_ALL_USERS_READY_TO_PLAY");
  
}

const changeBlock = (listOfUsers, userName) => {
  let currUser = {};
  const usernamesArray = listOfUsers.map(user => {
    if(user.username === userName)
    {
      currUser = user;

    }
    return user.username
  });
  const index = usernamesArray.indexOf(userName);
  const statusBlock = document.getElementsByClassName("player-status")[index];
  if(currUser.status === "not-ready") {
    statusBlock.classList.remove("ready");
    statusBlock.classList.add("not-ready");
  }
  else if(currUser.status === "ready") {
    statusBlock.classList.remove("not-ready");
    statusBlock.classList.add("ready");
  }
}
const drawText = (text, nextLetterId, typed) => {
  typedText.innerText = typed;
  if(!text[nextLetterId]){
    nextLetter.innerText = ""
  }
  else
  {
    nextLetter.innerText = text[nextLetterId];
  }
  restText.innerText = text.substr(nextLetterId + 1);
}
const drawIndicator = (progress, listOfUsers, username) => {
  const usernamesArray = listOfUsers.map(user => {
    return user.username
  });
  const index = usernamesArray.indexOf(username);
  const progressIndicator = document.getElementsByClassName("progress-indicator")[index];
  if(progress === 100)
  {
    progressIndicator.style.backgroundColor = "#00FF00"
  }
  progressIndicator.style.width = progress + "%"
}

const onKeyPressed = (ev) =>{
  socket.emit("TYPING_TEXT", ev.key);
}
const onCloseModal = (ev) => {
  console.log("hhh");
  ratingModal.innerHTML = "";
  ratingModal.classList.add("display-none");
  readyButton.classList.remove("display-none");
  readyButton.innerText = "READY";
  socket.emit("CLEAR_RATING");

}
closeButton.addEventListener("click", onCloseModal);

const onReadyButtonClicked = () => {
  const state = readyButton.textContent;
  socket.emit("READY_NOT_READY_STATE", state);
}

const clearAfterGame = (userList) => {
  userList.forEach((user, index) => {
    const statusBlock = document.getElementsByClassName("player-status")[index];
    timerForGame.innerText = "";
    timerForGame.classList.add("display-none");
    statusBlock.classList.remove("ready");
    statusBlock.classList.add("not-ready");
    const progressIndicator = document.getElementsByClassName("progress-indicator")[index];
    progressIndicator.style.width = 0 + "%"
    progressIndicator.style.backgroundColor = "#138c08"
   })
    typedText.innerText = "";
    nextLetter.innerText = "";
    restText.innerText = "";
  textField.classList.add("display-none");
}
const onDisconnectButtonClicked = () => {
  socket.emit("disconnect");
  sessionStorage.removeItem("username");
  window.location.replace("/login");
}

const setTimerBeforeGame = (seconds) => {
  readyButton.classList.add("display-none");
  disconnectButton.classList.add("display-none");
  timerBeforeStart.classList.remove("display-none");
  renderSecondsLeft(seconds);
}
const setGameTime = (seconds) => {
  timerForGame.classList.remove("display-none");
  renderGameSecondsLeft(seconds);
}

const renderGameSecondsLeft = (seconds) => {
  timerForGame.innerText = `${seconds} left`;
}
const endGame = (userList) => {
  window.removeEventListener("keyup", onKeyPressed);
  clearAfterGame(userList);
  ratingModal.classList.remove("display-none");
  ratingModal.appendChild(closeButton);
  userList.forEach((user, index) =>{
    ratingModal.appendChild(createElement("span", "", index + ". " + user.username));
    ratingModal.appendChild(createElement("br", "", ""));
  })
}
const endGameTime = (ratingList, userList) => {
  window.removeEventListener("keyup", onKeyPressed);
  clearAfterGame(userList);
  ratingModal.classList.remove("display-none");
  ratingModal.appendChild(closeButton);
  ratingList.forEach((user, index) =>{
    ratingModal.appendChild(createElement("span", "", index + ". " + user[0]));
    ratingModal.appendChild(createElement("br", "", ""));
  })
} 
const setText = (textId) => {
  window.addEventListener("keyup", onKeyPressed);
  let textForTyping = "";
  timerBeforeStart.classList.add("display-none");
  getTextForGameById(textId)
    .then(response => {
      textForTyping = response.text;
      renderFieldForGame(textForTyping);
    })
    .catch(error => console.log(error))

}
const renderFieldForGame = (text) => {
  textField.classList.remove("display-none");
  nextLetter.innerText = text[0];
  restText.innerText = text.substr(1);
  socket.emit("START_GAME", text);
} 
const renderSecondsLeft = (seconds) => {
  timerBeforeStart.innerText = seconds;
}

readyButton.addEventListener("click", onReadyButtonClicked);

disconnectButton.addEventListener("click", onDisconnectButtonClicked)


socket.on("USERNAME_ERROR", usernameISNotValid);
socket.on("ROOM_ERROR", roomLimitExceeded);
socket.on("RENDER_LIST_OF_USERS", renderUsers);
socket.on("RENDER_LIST_OF_USERS_NEW", renderNewUsers);
socket.on("ADD_NEW_USER", addNewUser);
socket.on("CHANGE_BUTTON_STATE", changeButton);
socket.on("CHANGE_STATUS_BLOCK", changeBlock);
socket.on("SET_TIMER_BEFORE_START", setTimerBeforeGame);
socket.on("RENDER_SECONDS_LEFT", renderSecondsLeft);
socket.on("SET_TEXT", setText);
socket.on("SET_GAME_TIMER", setGameTime);
socket.on("RENDER_GAME_SECONDS_LEFT", renderGameSecondsLeft);
socket.on("DRAW_TEXT", drawText);
socket.on("DRAW_INDICATOR", drawIndicator);
socket.on("END_GAME", endGame);
socket.on("END_GAME_TIME", endGameTime);